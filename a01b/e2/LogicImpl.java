package a01b.e2;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class LogicImpl implements Logic {

	private List<Integer> values;
	private int cont;
	
	public LogicImpl(int size) {
		values=new LinkedList<>();
		initializeValues(size);
	}
	@Override
	public boolean hit(int i) {
		cont ++;
		return Collections.min(values) == values.get(i);
	}

	@Override
	public int getValue(int i) {
		int val = values.get(i);
		values.set(i, Integer.MAX_VALUE);
		return val;
	}
	
	@Override
	public int nAttempts() {
		return this.cont;
	}
	
	private void initializeValues(int size) {
		Random r = new Random();
		for(int i=0;i<size;i++) {
			values.add(r.nextInt(99));
		}
	}
	
	
	
	

}
