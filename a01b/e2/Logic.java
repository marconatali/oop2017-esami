package a01b.e2;

public interface Logic {
	boolean hit(int i);
	int getValue(int i);
	int nAttempts();
}
