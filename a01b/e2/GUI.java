package a01b.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import javax.swing.*;

public class GUI extends JFrame{
	
	private final List<JButton> buttons;
	private Logic logic;
	
	public GUI(int size){		
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		buttons = new LinkedList<JButton>();
		logic = new LogicImpl(size);
		
		ActionListener ac = (src)-> {
			JButton b =(JButton)src.getSource();
			if(logic.hit(buttons.indexOf(b))) {
				int val=logic.getValue(buttons.indexOf(b));
				b.setEnabled(false);
				b.setText(String.valueOf(val));
				System.out.println("VALORE: " + val);
			}
			
			System.out.println("NUMERO DI TENTATIVI EFFETTUATI " + logic.nAttempts());
			
		};
		for(int i=0;i<size;i++) {
			final JButton bt = new JButton();
			bt.addActionListener(ac);
			bt.setText("*");
			buttons.add(bt);
			this.getContentPane().add(bt);
		}		
		
		this.setVisible(true);
	}
	
}
