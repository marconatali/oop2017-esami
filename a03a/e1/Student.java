package a03a.e1;

import java.util.LinkedList;
import java.util.List;

public class Student {
	private final String name;
	private final int studentId;
	
	private final List<Registration> registrations;

	public Student(String name, int studentId) {
		this.name = name;
		this.studentId = studentId;
		registrations = new LinkedList<>();
	}

	public String getName() {
		return name;
	}

	public int getStudentId() {
		return studentId;
	}

	public List<Registration> getRegistrations() {
		return registrations;
	}
}
