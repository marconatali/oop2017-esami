package a03a.e1;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class ExamsManagementImpl implements ExamsManagement {

	LinkedList<Exam> exams;
	List<Student> students;
	
	public ExamsManagementImpl() {
		exams = new LinkedList<>();
		students = new LinkedList<>();
	}
	@Override
	public void createStudent(int studentId, String name) {
		students.add(new Student(name, studentId));
	}

	@Override
	public void createExam(String examName, int incrementalId) {
		check(incrementalId < exams.getLast().getExamId());
		exams.add(new Exam(incrementalId, examName));

	}

	@Override
	public void registerStudent(String examName, int studentId) {
		Optional<Student> stud = this.getSttudentFromId(studentId);
		Optional<Exam> exam = this.getExamFromName(examName);
		check(stud.isPresent());
		check(exam.isPresent());
		stud.get().getRegistrations().add(new Registration(studentId,exam.get().getExamId()));
	}

	@Override
	public void examStarted(String examName) {
		Optional<Exam> exam = this.getExamFromName(examName);
		check(exam.isPresent());
		check(exam.stream().filter(e->e.isStarted()).findFirst().isEmpty());
		exam.get().setStarted(true);
	}

	@Override
	public void registerEvaluation(int studentId, int evaluation) {
		Optional<Student> stud = this.getSttudentFromId(studentId);
		check(stud.isPresent());
	}

	@Override
	public void examFinished() {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<Integer> examList(String examName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Integer> lastEvaluation(int studentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Integer> examStudentToEvaluation(String examName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Integer, Integer> examEvaluationToCount(String examName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private Optional<Student> getSttudentFromId(int studentId){
		return students.stream().filter(s->s.getStudentId()==studentId).findFirst();
	}
	
	private Optional<Exam> getExamFromName(String examName){
		return exams.stream().filter(e->e.getName().equals(examName)).findFirst(); 
	}
	
	private void check(boolean b) {
		if(!b) {
			throw new IllegalStateException();
		}
	}

}
