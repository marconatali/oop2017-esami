package a03a.e1;

public class Exam {
	private final int examId;
	private final String name;
	private boolean started;
	
	public Exam(int examId, String name) {
		this.examId = examId;
		this.name = name;
		this.setStarted(false);
	}

	public int getExamId() {
		return examId;
	}

	public String getName() {
		return name;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}	
}
