package a01a.e2;

public interface Logic {
	boolean hit(int i);
	boolean isOver();
	int getValue(int i);
	
}
