package a01a.e2;


import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;



public class GUI extends JFrame{
	
	private final Map<JButton,Integer> buttons;
	private Logic logic;
	
	public GUI(int size){
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		buttons=new HashMap<>();
		logic = new LogicImpl(size);
	
		ActionListener ac = e -> {
			final JButton jb = (JButton)e.getSource();			
			if(logic.hit(buttons.get(jb))) {
				if(!logic.isOver()) {
					jb.setText(String.valueOf(logic.getValue(buttons.get(jb))));
					printButtons();
				}else {
					System.exit(0);
				}
			}else {
				jb.setEnabled(false);
			}		
		};
		
		ActionListener ap = e -> {
			logic = new LogicImpl(size);
			initializeButtons();
		};
		
		for (int i=0; i<size; i++){            
            final JButton bt = new JButton();
            bt.addActionListener(ac);
            buttons.put(bt, i);
            this.getContentPane().add(bt);
        }
		initializeButtons();
		
		final JButton print = new JButton("Print");
        print.addActionListener(ap);
        this.getContentPane().add(print);      
		
		this.setVisible(true);
	}
	
	private void initializeButtons() {
		for(JButton jb : buttons.keySet()) {
			jb.setText(String.valueOf(logic.getValue(buttons.get(jb))));
			jb.setEnabled(true);
		}
	}
	
	private void printButtons() {
		for(JButton bt : buttons.keySet()) {
			System.out.print(bt.getText() + "| " );
		}
	}

}
