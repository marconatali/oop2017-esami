package a01a.e2;

import java.util.LinkedList;
import java.util.List;

public class LogicImpl implements Logic {

	private final int size;
	private List<Integer> values;
	
	public LogicImpl(int size) {
		this.size=size;
		values=new LinkedList<Integer>();
		for(int i=0; i<size; i++) {
			values.add(0);
		}
	}
	@Override
	public boolean hit(int i) {
		values.set(i, values.get(i)+1);
		return values.get(i)<size;
	}

	@Override
	public int getValue(int i) {
		return this.values.get(i);
	}
	@Override
	public boolean isOver() {
		for(int i=0;i<size;i++) {
			if(values.get(i).equals(values.get(i+1))) {
				return false;
			}
		}
		return true;
	}

}
