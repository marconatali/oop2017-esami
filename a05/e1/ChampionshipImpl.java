package a05.e1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ChampionshipImpl implements Championship {

	private List<String> teams = new ArrayList<>();
	private List<Match> calendar = new ArrayList<>();
	private Set<Match> day=new HashSet<>();
	private final Map<String,Integer> classification = new HashMap<>();
	private boolean completed=false;
	
	@Override
	public void registerTeam(String name) {
		check(!completed);
		teams.add(name);
	}

	private void check(boolean b) {
		if(!b) {
			throw new IllegalStateException();
		}
	}

	@Override
	public void startChampionship() {
		for(List <String> matchTeams : Combinations.combine(teams)) {
			calendar.add(new MatchImpl(matchTeams.get(0), matchTeams.get(1)));
		}
		for(String team : teams) {
			classification.put(team, 0);
		}
		completed=true;
	}

	@Override
	public void newDay() {
		for(int i=0; i< teams.size()/2;i++) {
			day.add(calendar.remove(0));
		}
	}

	@Override
	public Set<Match> pendingMatches() {
		return this.day;
	}

	@Override
	public void matchPlay(Match match, int homeGoals, int awayGoals) {
		day.remove(match);
		int homePoints=classification.get(match.getHomeTeam());
		int awayPoints=classification.get(match.getAwayTeam());
		int diff=homeGoals-awayGoals;
		if(diff>0) {
			homePoints+=3;
		}else if(diff<0) {
			awayPoints+=3;
		} else {
			homePoints++;
			awayPoints++;
		}
		
		classification.replace(match.getHomeTeam(),homePoints);
		classification.replace(match.getAwayTeam(),awayPoints);
	}

	@Override
	public Map<String, Integer> getClassification() {
		return this.classification;
	}

	@Override
	public boolean championshipOver() {
		return calendar.isEmpty() && this.day.isEmpty();
	}

}
