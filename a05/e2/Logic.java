package a05.e2;

public interface Logic {
	void hit();
	int getResA();
	int getResB();
	int getPointsA();
	int getPointsB();
	boolean isOver();
}
