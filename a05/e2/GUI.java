package a05.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.*;



public class GUI extends JFrame{
	
	private final List<JButton> buttonsA;
	private final List<JButton> buttonsB;
	private Logic logic;
	
	public GUI(int size){
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout());
		JPanel jNorth = new JPanel();
		JPanel jSouth = new JPanel();
		JPanel jCenter = new JPanel();
		this.getContentPane().add(BorderLayout.NORTH, jNorth);
		this.getContentPane().add(BorderLayout.SOUTH, jSouth);
		this.getContentPane().add(BorderLayout.CENTER, jCenter);
		
		buttonsA = new LinkedList<>();
		buttonsB = new LinkedList<>();
		logic=new LogicImpl(size, size);
		
		for(int i=0;i<size;i++) {
			final JButton a=new JButton("*");
			a.setEnabled(false);
			jNorth.add(a);
			buttonsA.add(a);
			
			final JButton b=new JButton("*");
			b.setEnabled(false);
			jSouth.add(b);
			buttonsB.add(b);
		}
		
		final JButton p = new JButton("Play");
		p.addActionListener(e-> {
			logic.hit();
			System.out.println("{A=" + logic.getResA() + ", B=" + logic.getResB() + " }" );
			if(!logic.isOver()) {
				draw(buttonsA,logic.getPointsA());
				draw(buttonsB,logic.getPointsB());
			}else {
				System.exit(0);
			}
			
		});
		jCenter.add(p);
		
		this.pack();
		this.setVisible(true);
	}

	private void draw(List<JButton> buttons, int points) {
		if(points<buttons.size()) {
			buttons.get(points).setText(" ");
		}			
	}
	
}

