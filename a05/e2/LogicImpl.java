package a05.e2;

import java.util.Random;

public class LogicImpl implements Logic {

	private int pointsA;
	private int pointsB;
	private int resA;
	private int resB;
	
	public LogicImpl(int pointsA, int pointsB) {
		this.pointsA=pointsA;
		this.pointsB=pointsB;
	}
	
	@Override
	public void hit() {
		Random r = new Random();
		resA=r.nextInt(6)+1;
		resB=r.nextInt(6)+1;
		
		if(resA<resB) {
			pointsA--;
		}else if (resA == resB) {
			pointsA--;
			pointsB--;
		}else {
			pointsB--;
		}
	}

	@Override
	public int getResA() {
		return this.resA;
	}

	@Override
	public int getResB() {
		return this.resB;
	}

	@Override
	public int getPointsA() {
		return this.pointsA;
	}

	@Override
	public int getPointsB() {
		return this.pointsB;
	}

	@Override
	public boolean isOver() {
		return this.pointsA == 0 || this.pointsB == 0;
	}

}
