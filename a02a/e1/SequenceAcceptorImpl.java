package a02a.e1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SequenceAcceptorImpl implements SequenceAcceptor {

	private Map <Sequence,Iterable<Integer>> sequences;
	private Pair<Sequence,Iterator<Integer>> current;
	
	public SequenceAcceptorImpl() {
		sequences = new HashMap<>();
		sequences.put(Sequence.POWER2, ()->IntStream.iterate(1,i-> i*2).iterator());
		sequences.put(Sequence.FLIP, ()->IntStream.iterate(1, i -> (i+1)%2).iterator());
		sequences.put(Sequence.RAMBLE, ()->IntStream.iterate(1,i->i+1).map(i->i%2==0 ? i/2 : 0).iterator());
		sequences.put(Sequence.FIBONACCI, ()->Stream.iterate(new Pair<>(1,1),p-> new Pair<>(p.getY(), p.getX() + p.getY()))	
				.map(Pair::getX)
				.iterator());
	}

	@Override
	public void reset(Sequence sequence) {
		this.current=new Pair<>(sequence, sequences.get(sequence).iterator());
	}

	@Override
	public void reset() {
		check(this.current!=null);
		this.reset(this.current.getX());
	}

	@Override
	public void acceptElement(int i) {
		check(this.current!=null);
		check(current.getY().hasNext());
		check(current.getY().next()==i);
	}
	
	private void check(boolean b) {
		if(!b) {
			throw new IllegalStateException();
		}
	}

}
