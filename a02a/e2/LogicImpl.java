package a02a.e2;

import java.util.HashSet;
import java.util.Set;

public class LogicImpl implements Logic {

	private Set<Pair<Integer,Integer>> hittedButtons;
	private int size;
	
	public LogicImpl(int size) {
		hittedButtons=new HashSet<>();
		this.size=size;
	}

	@Override
	public boolean hit(int x, int y) {
		
		return hittedButtons.contains(new Pair<>(x,y)) ? !hittedButtons.remove(new Pair<>(x,y)) : hittedButtons.add(new Pair<>(x,y));
	}

	@Override
	public boolean isOver() {
		int contRow=0;
		int contCol=0;
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				if(hittedButtons.contains(new Pair<>(i, j))) {
					contRow ++;
				}
				if(hittedButtons.contains(new Pair<>(j, i))) {
					contCol ++;
				}
			}
			if(contRow==size || contCol==size) {
				return true;
			}
			contRow=0;
			contCol=0;
		}
		return false;
		
	}

}
