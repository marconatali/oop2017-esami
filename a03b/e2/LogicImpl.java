package a03b.e2;

import java.util.HashSet;
import java.util.Set;

public class LogicImpl implements Logic {

	private Set<Pair<Integer,Integer>> hittedButtons;
	int size;
	
	public LogicImpl(int size) {
		hittedButtons=new HashSet<>();
		this.size=size;
	}
	
	@Override
	public void hit(int x, int y) {
		for(int i=0; i<size;i++) {
			for(int j=0;j<size;j++) {
				if((i+j)==(x+y)) {
					Pair<Integer,Integer> coord = new Pair<>(i,j);
					if(hittedButtons.contains(coord)) {
						hittedButtons.remove(coord);
					} else {
						hittedButtons.add(coord);
					}					
				}
			}
		}
	}

	@Override
	public boolean getStatus(int x, int y) {
		Pair<Integer,Integer> coord = new Pair<>(x,y);
		return hittedButtons.contains(coord);
	}

}
