package a03b.e2;

public interface Logic {
	void hit(int x, int y);
	boolean getStatus(int x,int y);
}
